//Arduino Mega 1280 5v


#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);
#define SERIAL_BUFFER_SIZE 64

// Radio Library
#include "Arduino.h"
#include <RF24.h>
#include <nRF24L01.h>
#include <stdlib.h>
#define PLOAD_WIDTH 32 // 32 unsigned chars TX payload

// This is just the way the RF24 library works:
// Hardware configuration: Set up nRF24L01 radio on SPI bus (pins 50, 51, 52) plus pins 7 & 8
RF24 radio(7, 8);
byte pip;
byte pload_width_now;
byte newdata;
unsigned char rx_buf[PLOAD_WIDTH] = {0};

unsigned char ADDRESS2[1]= {0xb2};
unsigned char ADDRESS3[1]= {0xb3};
unsigned char ADDRESS4[1]= {0xb4};
unsigned char ADDRESS5[1]= {0xb5};

unsigned char ADDRESS1[5] = {0xb1, 0x43, 0x88, 0x99, 0x45};//Define a static RX address
unsigned char ADDRESS0[5] = {0xb1, 0x43, 0x88, 0x99, 0x45};//Define a static TX address



// For sensor read out
float dataVDO[3]; // [0]oil temperature, [1]oil pressure, [2]resistor value to calculate
float dataBatt; //Battery Voltage
byte countVDO = 0; // To keep track of successful msg receiveed
byte countBatt = 0;
int temp = 0;

// *** Declare the 4067 MUX pins on Mega
int s0 = 17;
int s1 = 16;
int s2 = 15;
int s3 = 14;



void setup() {

  //setting up the OLED display
  // All OLED have the same I2C address so need to be initialize one by one
  
  pinMode(s0, OUTPUT);
  pinMode(s1, OUTPUT);
  pinMode(s2, OUTPUT);
  pinMode(s3, OUTPUT);
  digitalWrite(s0, LOW);// using the MUX address 0000
  digitalWrite(s1, LOW);
  digitalWrite(s2, LOW);
  digitalWrite(s3, LOW);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  digitalWrite(s0, HIGH);// using the MUX address 0001
  digitalWrite(s1, LOW);
  digitalWrite(s2, LOW);
  digitalWrite(s3, LOW);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  digitalWrite(s0, LOW);// using the MUX address 0010
  digitalWrite(s1, HIGH);
  digitalWrite(s2, LOW);
  digitalWrite(s3, LOW);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  Serial.begin(9600);
  Serial.println(F("THIS IS THE RECEIVER CODE - YOU NEED THE OTHER ARDUINO TO TRANSMIT"));
  
  // Initiate the radio object
  radio.begin();

  // Set the transmit power to high to balance power supply and range.
  radio.setPALevel(RF24_PA_HIGH);

  // Set the speed of the transmission to the quickest available
  radio.setDataRate(RF24_2MBPS);

  // Use a channel unlikely to be used by Wifi, Microwave ovens etc
  radio.setChannel(124);

  //radio.enableDynamicPayloads();
  // Open a writing and reading pipe on each radio, with opposite addresses
  radio.openWritingPipe(ADDRESS0);
  radio.openReadingPipe(0, ADDRESS0);
  radio.openReadingPipe(1, ADDRESS1);
  radio.openReadingPipe(2, ADDRESS2);
  //radio.openReadingPipe(3, ADDRESS3);
  //radio.openReadingPipe(4, ADDRESS4);
  //radio.openReadingPipe(5, ADDRESS5);  

  // Start the radio listening for data
  radio.startListening();

  dataVDO[2] = 1000.0;
}




// -----------------------------------------------------------------------------
// We are LISTENING on this device only (although we do transmit a response)
// -----------------------------------------------------------------------------
void loop(){
    // This is what we receive from the other device (the transmitter)
    // Is there any data for us to get?
    // PipeNum == 1 is battery channel, PipeNum == 2 is VDO pressure/temperature channel
    uint8_t pipeNum;
    if (radio.available(&pipeNum)) { 
        if (pipeNum == 1){
        // Go and read the data and put it into that variable
            radio.read( &dataBatt, sizeof(dataBatt));
            //Serial.print("Got data on pipe ");
            //Serial.println(pipeNum);
            //Serial.println(dataBatt);
            countBatt = 0;
     
        }
        if (pipeNum == 2){
            radio.read( &dataVDO, sizeof(dataVDO));
            Serial.print("Got data on pipe ");
            Serial.print(pipeNum);
            //Serial.println(dataVDO[0]);
            Serial.print(" ");
            Serial.println(dataVDO[1]); //VDO temp
            //Serial.print(" ");
            Serial.println(dataVDO[2]);
            countVDO = 0;   
          
        }
    }

    // If we lost the signal more than 5 tries then reset display to 0
    if (countBatt > 5){
      dataBatt = 0; //set Battery Voltage to 0
    }
    
    if (countVDO > 5){
      dataVDO[0] = 0; //set oil temperature to 0
      dataVDO[1] = 0; //set oil pressure to 0
    }
    
    // No more data to get so send it back
    // First, stop listening so we can talk
    radio.stopListening();

    //display1(data[0]); //Displays Oil pressure
    enginePressure(dataVDO[0]);
    engineTemp(dataVDO[1], dataVDO[2]);
    batteryVoltage(dataBatt);

    // Now, resume listening so we catch the next packets.
    countVDO++;
    countBatt++;
    radio.startListening();
    delay(100);
    
}

// *** For VDO pressure sender ***
void enginePressure(float raw){
    if (raw == 0){
        errorDisplay(LOW, HIGH, F("Engine Oil Pressure"), F("PSI"), F("Error!"));
    }
    else {
        float Vin = 4.95; 
        float Vout = 0;
        float R1 = 506;
        float R2 = 0;
        float psi = 0;
        float buffer = 0; //buffer = ratio of Vout/Vin
        //Serial.println(raw);
        buffer = raw * Vin;
        Vout = (buffer)/1024.0;
        buffer = (Vin/Vout) - 1;
        R2 = R1 / buffer;
        //Serial.println(R2);
        //inverse of quadratic formula, and then convert bar to psi
        psi = ((-36.465 + sqrt(1329.696 + 1.4728*(10.648-R2))) / -0.7364) / 0.0689475728;
        //Serial.println(abs(psi));
        updateDisplay(LOW, HIGH, F("Engine Oil Pressure"), F("PSI"), abs(psi));
    }
}

// *** For VDO Temp sender ***
void engineTemp(float raw, float R1){
    if (raw == 0){
        errorDisplay(LOW, LOW, F("Engine Oil Temp"), F("Celcius"), F("Error!"));
    }
    else { 
        float Vin = 3.3; //the 5v on the Arduino is very dirty. use 3.3v instead. 
        float Vout = 0;
        //float R1 = 1000.0;  //choose a resistor value at where the temp is most critical. e.g. 85-100 degrees. ~ 100 ohms
        float R2 = 0; // resistance of the VDO themistor
        float buffer = 0; //buffer = ratio of Vout/Vin
        //Serial.println(raw);
        buffer = raw * Vin;
        Vout = (buffer)/1023.0;
        buffer = (Vin/Vout) - 1;
        R2 = R1 / buffer;
        //Serial.println(R2);
        updateDisplay(LOW, LOW, F("Engine Oil Temp"), F("Celcius"), tempLookupTable(R2));
    }
}

// *** For Battery voltage ***
void batteryVoltage(float volts){
    if (volts == 0){
        errorDisplay(HIGH, LOW, F("Battery Voltage"), F("Volts"), F("Error!"));
    }
    else {
        //*** For Battery Voltage ***
        //Serial.print("voltage: ");
        //Serial.print(volts/100.0);
        //Serial.println(volts);
        updateDisplay(HIGH, LOW, F("Battery Voltage"), F("Volts"), volts-0.15);
    }
}




void updateDisplay(bool bit0, bool bit1, String device, String unit, float value){  
//————-update display ————
    digitalWrite(s0, bit0);
    digitalWrite(s1, bit1);
    //digitalWrite(10, LOW);
    //digitalWrite(11, LOW);
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0,1);
    display.println(device);
    display.setTextColor(BLACK, WHITE);
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(0,16);
    display.println(unit);
    display.setCursor(0, 43);
    display.setTextSize(3);
    display.println(value);
    display.display();

}

void errorDisplay(bool bit0, bool bit1, String device, String unit, String msg){  
//————-update display ————
    digitalWrite(s0, bit0);
    digitalWrite(s1, bit1);
    //digitalWrite(10, LOW);
    //digitalWrite(11, LOW);
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.setCursor(0,1);
    display.println(device);
    display.setTextColor(BLACK, WHITE);
    display.setTextSize(2);
    display.setTextColor(WHITE);
    display.setCursor(0,16);
    display.println(unit);
    display.setCursor(0, 43);
    display.setTextSize(3);
    display.println(msg);
    display.display();

}

int tempLookupTable(float R2){
    if (R2 >= 36563.56){
        temp = -40;
    }
    else if (R2 >= 34507.77 && R2 < 36563.56){
        temp = -39;
    }
    else if (R2 >= 32451.99 && R2 < 34507.77){
        temp = -38;
    }        
    else if (R2 >= 30396.20 && R2 < 32451.99){
        temp = -37;
    }
    else if (R2 >= 28340.42 && R2 < 30396.20){
        temp = -36;
    }
    else if (R2 >= 26284.63 && R2 < 28340.42){
        temp = -35;
    }
    else if (R2 >= 24857.54 && R2 < 26284.63){
        temp = -34;
    }
    else if (R2 >= 23430.45 && R2 < 24857.54){
        temp = -33;
    }   
    else if (R2 >= 22003.37 && R2 < 23430.45){
        temp = -32;
    }
    else if (R2 >= 20576.28 && R2 < 22003.37){
        temp = -31;
    }
    else if (R2 >= 19149.2 && R2 < 20576.28){
        temp = -30;
    }
    else if (R2 >= 18144.90 && R2 < 19149.2){
        temp = -29;
    }
    else if (R2 >= 17140.59 && R2 < 18144.90){
        temp = -28;
    }
    else if (R2 >= 16136.29 && R2 < 17140.59){
        temp = -27;
    }
    else if (R2 >= 15131.98 && R2 < 16136.29){
        temp = -26;
    }
    else if (R2 >= 14127.68 && R2 < 15131.98){
        temp = -25;
    }
    else if (R2 >= 13410.28 && R2 < 14127.68){
        temp = -24;
    }
    else if (R2 >= 12692.88 && R2 < 13410.28){
        temp = -23;
    }
    else if (R2 >= 11975.48 && R2 < 12692.88){
        temp = -22;
    }
    else if (R2 >= 11258.08 && R2 < 11975.48){
        temp = -21;
    }
    else if (R2 >= 10540.68 && R2 < 11258.08){
        temp = -20;
    }
    else if (R2 >= 9976.81 && R2 < 10540.68){
        temp = -19;
    }
    else if (R2 >= 9412.95 && R2 < 9976.81){
        temp = -18;
    }
    else if (R2 >= 8849.08 && R2 < 9412.95){
        temp = -17;
    }
    else if (R2 >= 8285.22 && R2 < 8849.08){
        temp = -16;
    }
    else if (R2 >= 7721.35 && R2 < 8285.22){
        temp = -15;
    }
    else if (R2 >= 7321.26 && R2 < 7721.35){
        temp = -14;
    }
    else if (R2 >= 6921.16 && R2 < 7321.26){
        temp = -13;
    }    
    else if (R2 >= 6521.07 && R2 < 6921.16){
        temp = -12;
    }
    else if (R2 >= 6120.97 && R2 < 6521.07){
        temp = -11;
    }
    else if (R2 >= 5720.88 && R2 < 6120.97){
        temp = -10;
    }
    else if (R2 >= 5426.31 && R2 < 5720.88){
        temp = -9;
    }
    else if (R2 >= 5131.74 && R2 < 5426.31){
        temp = -8;
    }
    else if (R2 >= 4837.17 && R2 < 5131.74){
        temp = -7;
    }
    else if (R2 >= 4542.6 && R2 < 4837.17){
        temp = -6;
    }    
    else if (R2 >= 4248.03 && R2 < 4542.6){
        temp = -5;
    }    
    else if (R2 >= 4046.43 && R2 < 4248.03){
        temp = -4;
    }
    else if (R2 >= 3844.86 && R2 < 4046.43){
        temp = -3;
    }
    else if (R2 >=  3643.29 && R2 < 3844.86){
        temp = -2;
    }
    else if (R2 >= 3441.72 && R2 < 3643.29){
        temp = -1;
    }
    else if (R2 >= 3240.15 && R2 <3441.72){
        temp = 0;
    }
    else if (R2 >= 3086.85 && R2 < 3240.15){
        temp = 1;
    }
    else if (R2 >= 2933.55 && R2 < 3086.85){
        temp = 2;
    }
    else if (R2 >= 2708.25 && R2 < 2933.55){
        temp = 3;
    }
    else if (R2 >= 2626.95 && R2 < 2708.55){
        temp = 4;
    }
    else if (R2 >= 2473.65 && R2 < 2626.95){
        temp = 5;
    }
    else if (R2 >= 2360.05 && R2 < 2473.65){
        temp = 6;
    }
    else if (R2 >= 2246.51 && R2 < 2360.05){
        temp = 7;
    }
    else if (R2 >= 2132.96 && R2 < 2246.51){
        temp = 8;
    }
    else if (R2 >= 2019.42 && R2 < 2132.96){
        temp = 9;
    }
    else if (R2 >= 1905.87 && R2 < 2019.42){
        temp = 10;
    }
    else if (R2 >= 1822.03 && R2 < 1905.87 ){
        temp = 11;
    }
    else if (R2 >= 1738.18 && R2 < 1822.03 ){
        temp = 12;
    }
    else if (R2 >= 1654.34 && R2 < 1738.18 ){
        temp = 13;
    }
    else if (R2 >= 1570.49 && R2 < 1654.34 ){
        temp = 14;
    }     
    else if (R2 >= 1486.65 && R2 < 1570.49 ){
        temp = 15;
    }       
    else if (R2 >= 1423.05 && R2 < 1486.65 ){
        temp = 16;
    }    
    else if (R2 >= 1359.45 && R2 < 1423.05 ){
        temp = 17;
    }    
    else if (R2 >= 1295.84 && R2 < 1359.45 ){
        temp = 18;
    }    
    else if (R2 >= 1232.24 && R2 < 1295.84 ){
        temp = 19;
    }    
    else if (R2 >= 1168.64 && R2 < 1232.24 ){
        temp = 20;
    }    
    else if (R2 >= 1120.25 && R2 < 1168.64 ){
        temp = 21;
    }    
    else if (R2 >= 1071.87 && R2 < 1120.25 ){
        temp = 22;
    }    
    else if (R2 >= 1023.48 && R2 < 1071.87 ){
        temp = 23;
    }    
    else if (R2 >= 975.1 && R2 < 1023.48){
        temp = 24;
    }    
    else if (R2 >= 926.71 && R2 < 975.1 ){
        temp = 25;
    }    
    else if (R2 >= 889.36 && R2 < 926.71 ){
        temp = 26;
    }    
    else if (R2 >= 852.02 && R2 < 889.36 ){
        temp = 27;
    }    
    else if (R2 >= 814.67 && R2 < 852.02 ){
        temp = 28;
    }    
    else if (R2 >= 777.33 && R2 < 814.67 ){
        temp = 29;
    }    
    else if (R2 >= 739.98 && R2 < 777.33 ){
        temp = 30;
    }    
    else if (R2 >= 710.96 && R2 < 739.98 ){
        temp = 31;
    }    
    else if (R2 >= 681.95 && R2 < 710.96 ){
        temp = 32;
    }  
    else if (R2 >= 652.93 && R2 < 681.95 ){
        temp = 33;
    }      
    else if (R2 >= 623.92 && R2 < 652.93 ){
        temp = 34;
    }    
    else if (R2 >= 594.9 && R2 < 623.92 ){
        temp = 35;
    }    
    else if (R2 >= 572.23 && R2 < 594.9 ){
        temp = 36;
    }    
    else if (R2 >= 549.55 && R2 < 572.23 ){
        temp = 37;
    }    
    else if (R2 >= 526.88 && R2 < 549.55 ){
        temp = 38;
    }    
    else if (R2 >= 504.2 && R2 < 526.88 ){
        temp = 39;
    }    
    else if (R2 >= 481.53 && R2 < 504.2 ){
        temp = 40;
    }    
    else if (R2 >= 463.74 && R2 < 481.53 ){
        temp = 41;
    }    
    else if (R2 >= 445.95 && R2 < 463.74 ){
        temp = 42;
    }    
    else if (R2 >= 428.15 && R2 < 445.95 ){
        temp = 43;
    }    
    else if (R2 >= 410.36 && R2 < 428.15 ){
        temp = 44;
    }    
    else if (R2 >= 392.57 && R2 < 410.36 ){
        temp = 45;
    }    
    else if (R2 >= 378.49 && R2 < 392.57 ){
        temp = 46;
    }    
    else if (R2 >= 364.41 && R2 < 378.49 ){
        temp = 47;
    }    
    else if (R2 >= 350.33 && R2 < 364.41 ){
        temp = 48;
    }    
    else if (R2 >= 336.25 && R2 < 350.33 ){
        temp = 49;
    }    
    else if (R2 >= 322.17 && R2 < 336.25 ){
        temp = 50;
    }    
    else if (R2 >= 310.97 && R2 < 322.17 ){
        temp = 51;
    }    
    else if (R2 >= 299.78 && R2 < 310.97 ){
        temp = 52;
    }    
     else if (R2 >= 288.58 && R2 < 299.78 ){
        temp = 53;
    }    
    else if (R2 >= 277.39 && R2 < 288.58 ){
        temp = 54;
    }    
    else if (R2 >= 266.19 && R2 < 277.39 ){
        temp = 55;
    }    
    else if (R2 >= 257.19 && R2 < 266.19 ){
        temp = 56;
    }    
    else if (R2 >= 248.18 && R2 < 257.19 ){
        temp = 57;
    }    
    else if (R2 >= 239.18 && R2 < 248.18 ){
        temp = 58;
    }    
    else if (R2 >= 230.17 && R2 < 239.18 ){
        temp = 59;
    }    
    else if (R2 >= 221.17 && R2 < 230.17 ){
        temp = 60;
    }    
    else if (R2 >= 213.88 && R2 < 221.17 ){
        temp = 61;
    }    
    else if (R2 >= 206.59 && R2 < 213.88 ){
        temp = 62;
    }    
    else if (R2 >= 199.3 && R2 < 206.59 ){
        temp = 63;
    }    
    else if (R2 >= 192.01 && R2 < 199.3 ){
        temp = 64;
    }    
    else if (R2 >= 184.72&& R2 < 192.01 ){
        temp = 65;
    }     
    else if (R2 >= 178.83 && R2 < 184.72 ){
        temp = 66;
    }    
    else if (R2 >= 172.95 && R2 < 178.83 ){
        temp = 67;
    }    
    else if (R2 >= 167.06 && R2 < 172.95 ){
        temp = 68;
    }    
    else if (R2 >= 161.18 && R2 < 167.06 ){
        temp = 69;
    }    
    else if (R2 >= 155.29 && R2 < 161.18 ){
        temp = 70;
    }    
    else if (R2 >= 150.51 && R2 < 155.29 ){
        temp = 71;
    }    
    else if (R2 >= 145.73 && R2 < 150.51){
        temp = 72;
    }    
    else if (R2 >= 140.94 && R2 < 145.73){
        temp = 73;
    }    
    else if (R2 >= 136.16 && R2 < 140.94 ){
        temp = 74;
    }    
    else if (R2 >= 131.38 && R2 < 136.16 ){
        temp = 75;
    }    
    else if (R2 >= 127.52 && R2 < 131.38 ){
        temp = 76;
    }    
    else if (R2 >= 123.66 && R2 < 127.52 ){
        temp = 77;
    }    
    else if (R2 >= 119.8 && R2 < 123.66 ){
        temp = 78;
    }    
    else if (R2 >= 115.94 && R2 < 119.8 ){
        temp = 79;
    }    
    else if (R2 >= 112.08 && R2 < 115.94){
        temp = 80;
    }    
    else if (R2 >= 108.94 && R2 < 112.08 ){
        temp = 81;
    }    
    else if (R2 >= 105.81 && R2 < 108.94 ){
        temp = 82;
    }    
    else if (R2 >= 102.67 && R2 < 105.81 ){
        temp = 83;
    }    
    else if (R2 >= 99.54 && R2 < 96.4 ){
        temp = 84;
    }    
    else if (R2 >= 96.4  && R2 < 99.54 ){
        temp = 85;
    }    
    else if (R2 >= 93.71 && R2 < 96.4 ){
        temp = 86;
    }    
    else if (R2 >= 91.02 && R2 < 93.71){
        temp = 87;
    }    
    else if (R2 >= 88.34 && R2 < 91.02){
        temp = 88;
    }
    else if (R2 >= 85.65 && R2 < 88.34 ){
        temp = 89;
    }
    else if (R2 >= 82.96 && R2 < 85.65 ){
        temp = 90;
    }
    else if (R2 >= 80.66 && R2 < 82.96 ){
        temp = 91;
    }
    else if (R2 >= 78.35 && R2 < 80.66 ){
        temp = 92;
    }
    else if (R2 >= 76.05 && R2 < 78.35 ){
        temp = 93;
    }
    else if (R2 >= 73.74 && R2 < 76.05 ){
        temp = 94;
    }
    else if (R2 >= 71.44 && R2 < 73.74){
        temp = 95;
    }
    else if (R2 >= 69.54 && R2 < 71.44){
        temp = 96;
    }
    else if (R2 >= 67.63 && R2 < 69.54){
        temp = 97;
    }
    else if (R2 >= 65.73 && R2 < 67.63){
        temp = 98;
    }
    else if (R2 >= 63.82 && R2 < 65.73){
        temp = 99;
    }
    else if (R2 >= 61.92 && R2 < 63.82){
        temp = 100;
    }
    else if (R2 >= 60.34 && R2 < 61.92){
        temp = 101;
    }
    else if (R2 >= 58.76 && R2 < 60.34){
        temp = 102;
    }
    else if (R2 >= 57.17 && R2 < 58.76){
        temp = 103;
    }
    else if (R2 >= 55.59 && R2 < 57.17){
        temp = 104;
    }
    else if (R2 >= 54.01 && R2 < 55.59){
        temp = 105;
    }
    else if (R2 >= 52.66 && R2 < 54.01){
        temp = 106;
    }
    else if (R2 >= 51.30 && R2 < 52.66){
        temp = 107;
    }
    else if (R2 >= 49.95 && R2 < 51.30){
        temp = 108;
    }
    else if (R2 >= 48.59 && R2 < 49.95){
        temp = 109;
    }
    else if (R2 >= 47.24 && R2 < 48.59){
        temp = 110;
    }
    else if (R2 >= 46.08 && R2 < 47.24){
        temp = 111;
    }
    else if (R2 >= 44.91 && R2 < 46.08){
        temp = 112;
    }
    else if (R2 >= 43.75 && R2 < 44.91){
        temp = 113;
    }
    else if (R2 >= 42.58 && R2 < 43.75){
        temp = 114;
    }
    else if (R2 >= 41.42 && R2 < 42.58){
        temp = 115;
    }
    else if (R2 >= 40.44&& R2 < 41.425){
        temp = 116;
    }
    else if (R2 >= 39.46 && R2 < 40.44){
        temp = 117;
    }
    else if (R2 >= 38.47 && R2 < 39.46){
        temp = 118;
    }
    else if (R2 >= 37.49 && R2 < 38.47){
        temp = 119;
    }
    else if (R2 >= 36.51 && R2 < 37.49){
        temp = 120;
    }
    else if (R2 >= 35.68 && R2 < 36.51){
        temp = 121;
    }
    else if (R2 >= 34.86 && R2 < 35.68){
        temp = 122;
    }
    else if (R2 >= 34.03 && R2 < 34.86){
        temp = 123;
    }
    else if (R2 >= 33.21 && R2 < 34.03){
        temp = 124;
    }
    else if (R2 >= 32.38 && R2 < 33.21){
        temp = 125;
    }
    else if (R2 >= 31.67 && R2 < 32.38){
        temp = 126;
    }
    else if (R2 >= 30.95 && R2 < 31.67){
        temp = 127;
    }
    else if (R2 >=  30.24 && R2 < 30.95){
        temp = 128;
    }
    else if (R2 >=  29.52 && R2 < 30.24){
        temp = 129;
    }
    else if (R2 >=  28.81 && R2 < 29.52){
        temp = 130;
    }
    else if (R2 >= 28.19 && R2 < 28.81){
        temp = 131;
    }
    else if (R2 >= 27.57 && R2 < 28.19){
        temp = 132;
    }
    else if (R2 >=  26.94 && R2 < 27.57){
        temp = 133;
    }
    else if (R2 >= 26.32 && R2 < 26.94){
        temp = 134;
    }
    else if (R2 >= 25.7 && R2 < 26.32){
        temp = 135;
    }
    else if (R2 >= 25.16 && R2 < 25.7){
        temp = 136;
    }
    else if (R2 >= 24.62 && R2 < 25.16){
        temp = 137;
    }
    else if (R2 >= 24.08 && R2 < 24.62){
        temp = 138;
    }
    else if (R2 >= 23.54  && R2 < 24.08){
        temp = 139;
    }
    else if (R2 >= 23 && R2 < 23.54){
        temp = 140;
    }
    else if (R2 >= 22.53 && R2 < 23){
        temp = 141;
    }
    else if (R2 >= 22.06 && R2 < 22.53){
        temp = 142;
    }
    else if (R2 >= 21.60 && R2 < 22.06){
        temp = 143;
    }
    else if (R2 >= 21.13 && R2 < 21.60){
        temp = 144;
    }
    else if (R2 >= 20.66 && R2 < 21.13){
        temp = 145;
    }
    else if (R2 >= 20.25 && R2 < 20.66){
        temp = 146;
    }
    else if (R2 >=  19.83 && R2 < 20.25){
        temp = 147;
    }
    else if (R2 >=  19.42 && R2 < 19.83){
        temp = 148;
    }
    else if (R2 >=  19 && R2 < 19.42){
        temp = 149;
    }
    else if (R2 >=  18.59 && R2 < 19){
        temp = 150;
    }
    else if (R2 >=  18.22 && R2 < 18.59){
        temp = 151;
    }
    else if (R2 >= 17.85 && R2 < 18.22){
        temp = 152;
    }
    else if (R2 >= 17.48 && R2 < 17.85){
        temp = 153;
    }
    else if (R2 >= 17.11 && R2 < 17.48){
        temp = 154;
    }
    else if (R2 >= 16.74 && R2 < 17.11){
        temp = 155;
    }
    else if (R2 >= 16.41 && R2 < 16.74){
        temp = 156;
    }
    else if (R2 >= 16.09 && R2 < 16.41){
        temp = 157;
    }
    else if (R2 >=  15.76 && R2 < 16.09){
        temp = 158;
    }
    else if (R2 >=  15.44 && R2 < 15.76){
        temp = 159;
    }
    else if (R2 >= 15.11 && R2 < 15.44){
        temp = 160;
    }
    else if (R2 >= 14.82 && R2 < 14.82){
        temp = 161;
    }
    else if (R2 >=  14.53 && R2 < 14.82){
        temp = 162;
    }
    else if (R2 >= 14.24 && R2 < 14.53){
        temp = 163;
    }
    else if (R2 >= 13.95 && R2 < 14.24){
        temp = 164;
    }
    else if (R2 >= 13.66 && R2 < 13.95){
        temp = 165;
    }
    else if (R2 >= 13.40 && R2 < 13.66){
        temp = 166;
    }
    else if (R2 >= 13.15 && R2 < 13.40){
        temp = 167;
    }
    else if (R2 >= 12.89 && R2 < 13.15){
        temp = 168;
    }
    else if (R2 >= 12.64 && R2 < 12.89){
        temp = 169;
    }
    else if (R2 >= 12.38 && R2 < 12.64){
        temp = 170;
    }
    else if (R2 >= 12.15 && R2 < 12.38){
        temp = 171;
    }
    else if (R2 >= 11.93 && R2 < 12.15){
        temp = 172;
    }
    else if (R2 >= 11.70 && R2 < 11.93){
        temp = 173;
    }
    else if (R2 >= 11.48 && R2 < 11.70){
        temp = 174;
    }
    else if (R2 >= 11.25 && R2 < 11.48){
        temp = 175;
    }
    else if (R2 >= 11.05 && R2 < 11.25){
        temp = 176;
    }
    else if (R2 >= 10.85 && R2 < 11.05){
        temp = 177;
    }
    else if (R2 >= 10.65 && R2 < 10.85){
        temp = 178;
    }
    else if (R2 >= 10.45 && R2 < 10.65){
        temp = 179;
    }
    else if (R2 < 10.45){
        temp = 180;
    }
    return temp;
}
