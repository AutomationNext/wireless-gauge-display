#include "Arduino.h"
#include <SPI.h>
#include <RF24.h>
#include <nRF24L01.h>

// This is just the way the RF24 library works:
// Hardware configuration: Set up nRF24L01 radio on SPI bus (pins 10, 11, 12, 13) plus pins 7 & 8
RF24 radio(7, 8);

unsigned char ADDRESS1[5] = {0xb1, 0x43, 0x88, 0x99, 0x45};//Define a static RX address
unsigned char ADDRESS0[5] = {0xb1, 0x43, 0x88, 0x99, 0x45};//Define a static TX address

unsigned char ADDRESS2[5] = {0xb2, 0x43, 0x88, 0x99, 0x45};

// Define data size
float dataVDO[3];
int switchEn = 2; // digital pin 2 controls the analog switch 4066 pin 1E


// -----------------------------------------------------------------------------
// SETUP   SETUP   SETUP   SETUP   SETUP   SETUP   SETUP   SETUP   SETUP
// -----------------------------------------------------------------------------
void setup() {
  Serial.begin(9600);
  Serial.println("THIS IS THE TRANSMITTER CODE - YOU NEED THE OTHER ARDIUNO TO SEND BACK A RESPONSE");
  pinMode(switchEn, OUTPUT); //set pin mode to output
  dataVDO[2] = 1000.0; //between 0 and 60 degrees it's 1000 ohms.
  digitalWrite(switchEn, LOW); // open the switch 
  // Initiate the radio object
  radio.begin();

  // Set the transmit power to lowest available to prevent power supply related issues
  radio.setPALevel(RF24_PA_HIGH);

  // Set the speed of the transmission to the quickest available
  radio.setDataRate(RF24_2MBPS);

  // Use a channel unlikely to be used by Wifi, Microwave ovens etc
  radio.setChannel(124);

  // Open a writing and reading pipe on each radio
  radio.openWritingPipe(ADDRESS2);

  // Ensure we have stopped listening (even if we're not) or we won't be able to transmit
  radio.stopListening(); 
}

// -----------------------------------------------------------------------------
// LOOP     LOOP     LOOP     LOOP     LOOP     LOOP     LOOP     LOOP     LOOP
// -----------------------------------------------------------------------------
void loop() {
    dataVDO[0] = analogRead(1); // this reads the resisance in ohms. oil pressure sender
    dataVDO[1] = analogRead(0); // reads the resistance in ohms. oil temp sender. 
    Serial.print("Pressure: ");
    Serial.print(dataVDO[0]);
    Serial.print(" ");
    Serial.print("Temperature: ");
    Serial.print(dataVDO[1]);
    Serial.println();
    
    if (dataVDO[1] < 190.0) {
        digitalWrite(switchEn, HIGH); // close the switch
        dataVDO[2] = 130; //60 degrees and above it's 1000 ohm parallel with 100 ohms. 
        Serial.println("1E HIGH");
    }

    radio.write(&dataVDO, sizeof(dataVDO));

    delay(500);
}
