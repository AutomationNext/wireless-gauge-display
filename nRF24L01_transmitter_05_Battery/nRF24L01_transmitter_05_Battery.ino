#include "Arduino.h"
#include <SPI.h>
#include <RF24.h>
#include <nRF24L01.h>

// This is just the way the RF24 library works:
// Hardware configuration: Set up nRF24L01 radio on SPI bus (pins 10, 11, 12, 13) plus pins 7 & 8
RF24 radio(7, 8);

// Define a static TX address
// just change b1 to b2 or b3 to send to other pip on receiver
unsigned char ADDRESS0[5] = {0xb1, 0x43, 0x88, 0x99, 0x45};

// data to transmit
float dataBatt;

// -----------------------------------------------------------------------------
// SETUP   SETUP   SETUP   SETUP   SETUP   SETUP   SETUP   SETUP   SETUP
// -----------------------------------------------------------------------------
void setup() {
  Serial.begin(9600);
  Serial.println("THIS IS THE TRANSMITTER CODE - YOU NEED THE OTHER ARDIUNO TO SEND BACK A RESPONSE");

  // Initiate the radio object
  radio.begin();

  //radio.setRetries(3,5); // delay, count
  // Set the transmit power to high
  radio.setPALevel(RF24_PA_HIGH);

  // Set the speed of the transmission to the quickest available
  radio.setDataRate(RF24_2MBPS);

  // Use a channel unlikely to be used by Wifi, Microwave ovens etc
  radio.setChannel(124);

  // Open a writing and reading pipe on each radio, with opposite addresses
  radio.openWritingPipe(ADDRESS0);
  //radio.openReadingPipe(0, ADDRESS0);
 
  // Ensure we have stopped listening (even if we're not) or we won't be able to transmit
  radio.stopListening();
}

// -----------------------------------------------------------------------------
// LOOP     LOOP     LOOP     LOOP     LOOP     LOOP     LOOP     LOOP     LOOP
// -----------------------------------------------------------------------------
void loop() {
  // Generate a single random character to transmit
  //unsigned char data = random(0, 254);
    //unsigned char data = analogRead(A0);
    dataBatt = analogRead(A0);
    //Serial.println(dataBatt);
    dataBatt = (dataBatt /1023)*3.44;
    dataBatt = dataBatt/0.2;  //voltage divider ratio is R1 = 30k, R2 = 7.5k, ratio = 0.2
    Serial.println(dataBatt);
    
    
  // Did we manage to SUCCESSFULLY transmit that (by getting an acknowledgement back from the other Arduino)?
  // Even we didn't we'll continue with the sketch, you never know, the radio fairies may help us
  if (!radio.write( &dataBatt, sizeof(dataBatt) )) {
      Serial.println(F("No acknowledgement of transmission - receiving radio device connected?"));    
  }

  delay(500);
}
